@echo off
git add .
if not %errorlevel% == 0 goto err_add
git commit -am "%date%"
if not %errorlevel% == 0 goto err_commit
git push -u origin
if not %errorlevel% == 0 goto err_push
echo Finished uploading.
pause
exit /b 0

:err_add

echo Could not add data
pause
exit /b 1
:err_commit

echo Could not commit data
pause
exit /b 1
:err_push

echo Could not push data
pause
exit /b 1