@echo off
if "%1"=="1" (goto restarted)
echo Server manager started, press ANY KEY to look for UPDATES changes
pause>nul
start "" "update.bat" 1
exit /b 0

:restarted
java -Xmx1024M -Xms1024M -jar minecraft_server.1.14.4.jar
:: "nogui"
echo Server closed.
echo Press ANY KEY to UPLOAD/SAVE data
pause>nul
call upload.bat