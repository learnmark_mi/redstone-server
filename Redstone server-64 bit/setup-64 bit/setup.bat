@echo off
cd ..
"setup-64 bit\git\bin\git.exe" clone https://gitlab.com/learnmark_mi/redstone-server.git
if not %errorlevel% == 0 goto git_fail
cd redstone-server
start %cd%

exit /b 0
:git_fail
echo git fail
pause
exit /b 1